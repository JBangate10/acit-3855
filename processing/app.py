import connexion
from connexion import NoContent
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker
from apscheduler.schedulers.background import BackgroundScheduler
from base import Base
from stats import Stats
from datetime import datetime, timedelta
from flask import jsonify
import sqlite3
import requests
import yaml
import logging
import logging.config
from flask_cors import CORS
import os


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_config.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)


DB_ENGINE = create_engine("sqlite:///%s" % app_config["datastore"]["filename"])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def stats(stats):
    session = DB_SESSION()

    stats = Stats(stats["num_post_events"],
                  stats["num_interaction_events"],
                  stats["max_post_event_timestamp"],
                  stats["max_interaction_event_timestamp"],
                  datetime.strptime(stats['last_updated'],"%Y-%m-%dT%H:%M:%S"))

    session.add(stats)
    session.commit()
    session.close()


def populate_stats():
    """ Periodically update stats """

    try:
        if not os.path.exists(app_config['datastore']['filename']):
            logger.info(f"{app_config['datastore']['filename']} not found. Creating...")
            conn = sqlite3.connect(app_config['datastore']['filename'])
            cursor = conn.cursor()
            cursor.execute('''CREATE TABLE stats
                                (id INTEGER PRIMARY KEY ASC,
                                num_post_events INTEGER NOT NULL,
                                num_interaction_events INTEGER NOT NULL,
                                max_post_event_timestamp VARCHAR(100) NOT NULL,
                                max_interaction_event_timestamp VARCHAR(100) NOT NULL,
                                last_updated VARCHAR(100) NOT NULL)
                            ''')
            conn.commit()
            conn.close()
            logger.info('SQLite Database file created and initialized.')
        else:
            conn = sqlite3.connect(app_config['datastore']['filename'])
            cursor = conn.cursor()
    except Exception as e:
        logger.error(f"Faied to create database: {e}")
        return

    logger.info("Start Periodic Processing")

    # Read current stats from SQLite Database
    conn = sqlite3.connect(app_config["datastore"]["filename"])
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM stats ORDER BY last_updated DESC LIMIT 1')
    result = cursor.fetchone()

    # If stats does not exist, use default values
    if result:
        last_updated = datetime.strptime(result[5], "%Y-%m-%d %H:%M:%S.%f")
        num_post_events = result[1]
        num_interaction_events = result[2]
    else:
        last_updated = datetime(2000, 1, 1)
        num_post_events = 0
        num_interaction_events = 0


    # Get current datetime
    current_datetime = datetime.now()

    # Query two GET endpoints from the Data Store Service
    try:
        response_post = requests.get(app_config["eventstore"]["url"] + "/postEvent", params={"start_timestamp": last_updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ"), "end_timestamp": current_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")})
        response_interaction = requests.get(app_config["eventstore"]["url"] + "/interactionEvent", params={"start_timestamp": last_updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ"), "end_timestamp": current_datetime.strftime("%Y-%m-%dT%H:%M:%S.%fZ")})

        # Log INFO message with number of events received, log ERROR message if response code is not 200
        if response_post.status_code == 200 and response_interaction.status_code == 200:
            logger.info(f"Received {len(response_post.json()) + len(response_interaction.json())} events.")
        else:
            logger.error(f"Error fetching events. Post response code: {response_post.status_code}, Interaction response code: {response_interaction.status_code}")
            return
    except Exception as e:
        logger.error(f"Error fetching events: {e}")
        return

    session = DB_SESSION()
    try:
        for event in response_post.json():
            num_post_events += 1
            last_updated = current_datetime
        for event in response_interaction.json():
            num_interaction_events += 1
            last_updated = current_datetime
        
        new_stats = Stats(num_post_events, num_interaction_events, event['timestamp'], event['timestamp'], last_updated)
        session.add(new_stats)
    except Exception as e:
        session.rollback()
        logger.error(f"Error updating statistics: {e}")
    finally:
        session.commit()
        session.close()

    logger.info("Periodic Processing Ended")


def get_stats():
    logger.info("Request for statistics has started")

    conn = sqlite3.connect(app_config["datastore"]["filename"])
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM stats ORDER BY last_updated DESC LIMIT 1")
    result = cursor.fetchone()
    conn.close()

    if not result:
        logger.error("Statistics do not exist")
        return jsonify({"message": "Statistics do not exist"}), 404
    
    stats_dict = {
        "num_post_events": result[1],
        "num_interaction_events": result[2],
        "max_post_event_timestamp": result[3],
        "max_interaction_timestamp": result[4],
        "last_updated": result[5]
    }

    logger.debug(f"Current statistics: {stats_dict}")

    logger.info("Request for statistics has completed")

    return stats_dict, 200


def init_scheduler():
    """ Intialize scheduler """
    sched =  BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats,
                  'interval',
                  seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS']='Content-Type'

app.add_api("openapi.yaml",
            strict_validation=True,
            validate_responses=True)


if __name__ == "__main__":
    init_scheduler()
    app.run(port=8100, host="0.0.0.0")