import sqlite3
import os
script_dir = os.path.abspath(os.path.dirname(__file__))
db_path = os.path.join(script_dir, 'stats.sqlite')

conn = sqlite3.connect(db_path)

c = conn.cursor()
c.execute('''
            CREATE TABLE stats
            (id INTEGER PRIMARY KEY ASC,
             num_post_events INTEGER NOT NULL,
             num_interaction_events INTEGER NOT NULL,
             max_post_event_timestamp VARCHAR(100) NOT NULL,
             max_interaction_event_timestamp VARCHAR(100) NOT NULL,
             last_updated VARCHAR(100) NOT NULL)
            ''')

conn.commit()
conn.close()