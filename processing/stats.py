from sqlalchemy import Column, Integer, String, DateTime
import datetime
from base import Base

class Stats(Base):
    """ Processing Statistics """
    __tablename__ = "stats"

    id = Column(Integer, primary_key=True)
    num_post_events = Column(Integer, nullable=False)
    num_interaction_events = Column(Integer, nullable=False)
    max_post_event_timestamp = Column(String, nullable=False)
    max_interaction_event_timestamp = Column(String, nullable=False)
    last_updated = Column(DateTime, nullable=False)

    def __init__(self, num_post_events, num_interaction_events, max_post_event_timestamp, max_interaction_event_timestamp, last_updated):
        """ Initializes a processing statistics object """

        self.num_post_events = num_post_events
        self.num_interaction_events = num_interaction_events
        self.max_post_event_timestamp = max_post_event_timestamp
        self.max_interaction_event_timestamp = max_interaction_event_timestamp
        self.last_updated = last_updated
    
    def to_dict(self):
        """ Dictionary Representation of a statistics """

        dict = {}
        dict['num_post_events'] = self.num_post_events
        dict['num_interaction_events'] = self.num_interaction_events
        dict['max_post_event_timestamp'] = self.max_post_event_timestamp
        dict['max_interaction_event_timestamp'] = self.max_interaction_event_timestamp
        dict['last_updated'] = self.last_updated.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

        return dict