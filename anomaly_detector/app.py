import connexion
from connexion import NoContent
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from apscheduler.schedulers.background import BackgroundScheduler
from base import Base
from anomaly import Anomaly
from datetime import datetime, timedelta
from flask import jsonify
import sqlite3
import requests
import yaml
import json
import logging
import logging.config
from flask_cors import CORS
from kafka import KafkaConsumer
import os


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_config.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

with open("app_conf.yml", "r") as f:
    config = yaml.safe_load(f)


kafka_host = config["kafka"]["hostname"]
kafka_port = config["kafka"]["port"]
kafka_topic = config["kafka"]["topic"]


DB_ENGINE = create_engine("sqlite:///%s" % config["datastore"]["filename"])
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


consumer = KafkaConsumer(
    kafka_topic,
    bootstrap_servers=f"{kafka_host}:{kafka_port}",
    auto_offset_reset="earliest",
    enable_auto_commit=True,
    group_id="anomaly-detector-group",
    value_deserializer=lambda x: json.loads(x.decode("utf-8")),
)

def detect_anomaly(event):
    """Detects anomalies in the given event"""
    event_type = event.get("event_type")
    value = event.get("value")
    threshold = config["thresholds"].get(event_type)

    if value is not None and threshold is not None:
        if value > threshold:
            return Anomaly(
                event_id=event["id"],
                trace_id=event["trace_id"],
                event_type=event_type,
                anomaly_type="Too High",
                description=f"The value is too high. Value: {value}, Threshold: {threshold}",
                date_created=datetime.now(),
            )
        elif value < threshold:
            return Anomaly(
                event_id=event["id"],
                trace_id=event["trace_id"],
                event_type=event_type,
                anomaly_type="Too Low",
                description=f"The value is too low. Value: {value}, Threshold: {threshold}",
                date_created=datetime.now(),
            )
    return None

def save_anomaly(anomaly):
    """Saves the anomaly to the SQLite database"""
    session = DB_SESSION()
    session.add(anomaly)
    session.commit()
    session.close()

def consume_events():
    """Consumes events from Kafka and detects anomalies"""
    for message in consumer:
        event = message.value
        anomaly = detect_anomaly(event)
        if anomaly:
            save_anomaly(anomaly)



app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'

app.add_api('openapi.yaml',
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8000, host="0.0.0.0")