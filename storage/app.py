import connexion
from connexion import NoContent
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from operator import and_
from pykafka import KafkaClient
from pykafka.common import OffsetType
from threading import Thread
import datetime
import time
import mysql.connector
import pymysql
import yaml
import logging
import logging.config
import json
from postEvent import PostEvent
from interactionEvent import InteractionEvent
import os


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_config.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

Base = declarative_base()

DB_ENGINE = create_engine('mysql+pymysql://{}:{}@{}:{}/{}'.format(app_config['datastore']['user'], app_config['datastore']['password'], app_config['datastore']['hostname'], app_config['datastore']['port'], app_config['datastore']['db']))
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)


logger.info('Connecting to DB. Hostname: {}, Port: {}'.format(
    app_config['datastore']['hostname'],
    app_config['datastore']['port']
))


# Your functions here to handle your endpoints

#def receive_post_event(body):
#    ''' Receives a post event '''
#
#    session = DB_SESSION()
#
#    post_event = PostEvent(body['uuid'],
#                           body['postContent'],
#                           body['timestamp'],
#                           body['platform'],
#                           body['trace_id'])
#    
#    session.add(post_event)
#    session.commit()
#    session.close
#
#    logger.debug('Stored event postEvent request with a trace ID of {}'.format(body['trace_id']))
#    return NoContent, 201

#def receive_interaction_event(body):
#    ''' Receives an interaction event '''
#
#    session = DB_SESSION()
#
#    interaction_event = InteractionEvent(body['uuid'],
#                                         body['interactionType'],
#                                         body['timestamp'],
#                                         body['postUuid'],
#                                         body['trace_id'])
#    
#    session.add(interaction_event)
#    session.commit()
#    session.close()
#
#    logger.debug('Stored event interactionEvent request with a trace ID of {}'.format(body['trace_id']))
#    return NoContent, 201


def get_post_events(start_timestamp, end_timestamp):
    ''' Gets new post events between the start and end timestamps '''

    session = DB_SESSION()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")

    results = session.query(PostEvent).filter(
        and_(PostEvent.date_created >= start_timestamp_datetime, 
             PostEvent.date_created < end_timestamp_datetime))
    
    results_list = [event.to_dict() for event in results]
    session.close()

    logger.info("Query for Post Events after %s returns %d results" % 
                (start_timestamp, len(results_list)))
    
    return results_list, 200

def get_interaction_events(start_timestamp, end_timestamp):
    ''' Gets new interaction events between the start and end timestamp '''

    session = DB_SESSION()
    start_timestamp_datetime = datetime.datetime.strptime(start_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    end_timestamp_datetime = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")

    results = session.query(InteractionEvent).filter(
        and_(InteractionEvent.date_created >= start_timestamp_datetime,
            InteractionEvent.date_created < end_timestamp_datetime))
    
    results_list = [result.to_dict() for result in results]
    session.close()

    logger.info("Query for Interaction Events after %s returns %d results" %
                (start_timestamp, len(results_list)))

    return results_list, 200

def process_messages():
    ''' Placeholder for the function that processes Kafka messages '''
    hostname = "%s:%d" % (app_config["events"]["hostname"], app_config["events"]["port"])
    max_retries = app_config["events"]["max_retries"]
    retry_count = 0
    retry_sleep = app_config["events"]["sleep"]

    while retry_count < max_retries:
        try:
            client = KafkaClient(hosts=hostname)
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            logger.info("Successfully connected to Kafka.")
            break
        except Exception as e:
            logger.info("Connection to Kafka failed. Error: %s" % str(e))
            time.sleep(retry_sleep)
            retry_count += 1
    else:
        logger.error("Maximum retry count reached. Unable to connect to Kafka.")
        return NoContent,

    consumer = topic.get_simple_consumer(consumer_group=b'event_group',
                                        reset_offset_on_start=False,
                                        auto_offset_reset=OffsetType.LATEST)

    try:
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg_data = json.loads(msg_str)
            logger.info("Received Message: %s" % msg_data)

            type = msg_data["type"]
            payload = msg_data["payload"]

            if type == "PostEvent":
                store_post_event(payload)
            elif type == "InteractionEvent":
                store_interaction_event(payload)
            
            consumer.commit_offsets()
    except Exception as e:
        logger.error(f"Error processing message: {str(e)}")

def store_post_event(payload):
    session = DB_SESSION()

    post_event = PostEvent(
        uuid=payload['uuid'],
        postContent=payload['postContent'],
        timestamp=payload['timestamp'],
        platform=payload['platform'],
        trace_id=payload['trace_id']
    )

    session.add(post_event)
    session.commit()
    session.close()

    logger.debug('Stored event post_event from Kafka with a trace ID of {}'.format(payload['trace_id']))

def store_interaction_event(payload):
    session = DB_SESSION()

    interaction_event = InteractionEvent(
        uuid=payload['uuid'],
        interactionType=payload['interactionType'],
        timestamp=payload['timestamp'],
        postUuid=payload['postUuid'],
        trace_id=payload['trace_id']
    )

    session.add(interaction_event)
    session.commit()
    session.close()

    logger.debug('Stored event interaction_event from Kafka with a trace ID of {}'.format(payload['trace_id']))

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    # Start thread to continuously process Kafka messages in parallel with the API
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090, host="0.0.0.0")