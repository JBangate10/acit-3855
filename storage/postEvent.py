from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class PostEvent(Base):
    """ Post Event """

    __tablename__ = "postEvent"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(250), nullable=False)
    postContent = Column(String(250), nullable=False)
    timestamp = Column(String(100), nullable=False)
    platform = Column(String(250), nullable=False)
    trace_id = Column(String(250), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, uuid, postContent, timestamp, platform, trace_id):
        """ Initializes a post event """

        self.uuid = uuid
        self.postContent = postContent
        self.timestamp = timestamp
        self.platform = platform
        self.trace_id = trace_id
        self.date_created = datetime.datetime.now()
    
    def to_dict(self):
        """ Dictionary Representation of a post event """

        dict = {}
        dict['id'] = self.id
        dict['uuid'] = self.uuid
        dict['postContent'] = self.postContent
        dict['timestamp'] = self.timestamp
        dict['platform'] = self.platform
        dict['trace_id'] = self.trace_id
        #dict['date_created'] = self.date_created.isoformat()

        return dict