import mysql.connector


db_conn = mysql.connector.connect(host="acit3855-jb-cloudvm.westus3.cloudapp.azure.com", user="root", password="password", database="events_db")
db_cursor = db_conn.cursor()


db_cursor.execute('''
            DROP TABLE postEvent, interactionEvent
            ''')


db_conn.commit()
db_conn.close()