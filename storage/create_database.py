import sqlite3

conn = sqlite3.connect('events_database.sqlite')
c = conn.cursor()

c.execute('''
          CREATE TABLE postEvent 
          (id INTEGER PRIMARY KEY AUTOINCREMENT,
           uuid VARCHAR(250) NOT NULL,
           postContent VARCHAR(250) NOT NULL,
           timestamp VARCHAR(250) NOT NULL,
           platform VARCHAR(250) NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

c.execute('''
          CREATE TABLE interactionEvent
          (id INTEGER PRIMARY KEY AUTOINCREMENT,
           uuid VARCHAR(250) NOT NULL,
           interactionType VARCHAR(250) NOT NULL,
           timestamp VARCHAR(250) NOT NULL,
           postUuid TEXT NOT NULL,
           trace_id VARCHAR(250) NOT NULL,
           date_created VARCHAR(100) NOT NULL)
          ''')

conn.commit()
conn.close()