import mysql.connector


db_conn = mysql.connector.connect(host="acit3855-jb-cloudvm.westus3.cloudapp.azure.com", user="root", password="password", database="events_db")
db_cursor = db_conn.cursor()


db_cursor.execute('''
            CREATE TABLE postEvent
            (id INT NOT NULL AUTO_INCREMENT,
             uuid VARCHAR(250) NOT NULL,
             postContent VARCHAR(250) NOT NULL,
             timestamp VARCHAR(250) NOT NULL,
             platform VARCHAR(250) NOT NULL,
             trace_id VARCHAR(250) NOT NULL,
             date_created VARCHAR(100) NOT NULL,
             CONSTRAINT postEvent_pk PRIMARY KEY (id))
            ''')

db_cursor.execute('''
            CREATE TABLE interactionEvent
            (id INT NOT NULL AUTO_INCREMENT,
             uuid VARCHAR(250) NOT NULL,
             interactionType VARCHAR(250) NOT NULL,
             timestamp VARCHAR(250) NOT NULL,
             postUuid TEXT NOT NULL,
             trace_id VARCHAR(250) NOT NULL,
             date_created VARCHAR(100) NOT NULL,
             CONSTRAINT interactionEvent_pk PRIMARY KEY (id))
            ''')


db_conn.commit()
db_conn.close()