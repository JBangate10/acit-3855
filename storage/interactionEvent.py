from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class InteractionEvent(Base):
    """ Interaction Event """

    __tablename__ = "interactionEvent"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(250), nullable=False)
    interactionType = Column(String(250), nullable=False)
    timestamp = Column(String(100), nullable=False)
    postUuid = Column(String(250), nullable=False)
    trace_id = Column(String(250), nullable=False)
    date_created = Column(DateTime, nullable=False)

    def __init__(self, uuid, interactionType, timestamp, postUuid, trace_id):
        """ Initializes an interaction event """

        self.uuid = uuid
        self.interactionType = interactionType
        self.timestamp = timestamp
        self.postUuid = postUuid
        self.trace_id = trace_id
        self.date_created = datetime.datetime.now()
    
    def to_dict(self):
        """ Dictionary Representation of an interaction event """
        dict = {}
        dict['id'] = self.id
        dict['uuid'] = self.uuid
        dict['interactionType'] = self.interactionType
        dict['timestamp'] = self.timestamp
        dict['postUuid'] = self.postUuid
        dict['trace_id'] = self.trace_id
        #dict['date_created'] = self.date_created.isoformat()

        return dict