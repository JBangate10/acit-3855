import connexion
from connexion import NoContent
from pykafka import KafkaClient
from pykafka.common import OffsetType
import json
import logging
import logging.config
import yaml
from flask_cors import CORS
import requests
import os


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_config.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)


def get_post_event(index):
    """ Get post event from history """

    hostname = "%s:%d" % (app_config["events"]["hostname"],
                          app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config["events"]["topic"])]

    if topic is None:
        logger.error("Topic not found")
        return NoContent, 500

    consumer = topic.get_simple_consumer(reset_offset_on_start=True,
                                         consumer_timeout_ms=1000)
    logger.info("Retrieving Post Event at index %d" % index)

    try:
        count = 0
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg_data = json.loads(msg_str)

            if msg_data["type"] == "PostEvent":
                if count == index:
                    return msg_data["payload"], 200
                count += 1
    except Exception as e:
        logger.error(f"Error retrieving Post Event: {str(e)}")
    
    logger.error("Could not find Post Event at index %d" % index)
    return { "message": "Not Found" }, 404

def get_interaction_event(index):
    """ Get interaction event from history """

    hostname = "%s:%d" % (app_config["events"]["hostname"],
                          app_config["events"]["port"])
    client = KafkaClient(hosts=hostname)
    topic = client.topics[str.encode(app_config["events"]["topic"])]

    if topic is None:
        logger.error("Topic not found")
        return NoContent, 500

    consumer = topic.get_simple_consumer(reset_offset_on_start=True,
                                         consumer_timeout_ms=1000)
    logger.info("Retrieving Interaction Event at index %d" % index)

    try:
        count = 0
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg_data = json.loads(msg_str)

            if msg_data["type"] == "InteractionEvent":
                if count == index:
                    return msg_data["payload"], 200
                count += 1

    except Exception as e:
        logger.error(f"Error retrieving Interaction Event: {str(e)}")

    logger.error("Could not find Interaction Event at index %d" % index)
    return { "message": "Not Found" }, 404


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS']='Content-Type'

app.add_api("openapi.yaml",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8110, host="0.0.0.0")