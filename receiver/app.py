import yaml
import connexion
import requests
from connexion import NoContent
from pykafka import KafkaClient
import datetime
import time
import json
import os
import logging
import logging.config
import uuid


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yml"
    log_conf_file = "/config/log_conf.yml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yml"
    log_conf_file = "log_conf.yml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)


def kafka_producer_initialize():
    retry_sleep = app_config["events"]["sleep"]
    max_retries = app_config["events"]["max_retries"]
    retry_count = 0
    while max_retries > retry_count:
        try:
            logger.info("Attempting to connect to Kafka. Retry count: %d" % (retry_count + 1))
            client = KafkaClient(hosts=f'{app_config["events"]["hostname"]}:{app_config["events"]["port"]}')
            topic = client.topics[str.encode(app_config["events"]["topic"])]
            producer = topic.get_sync_producer()
            logger.info("Connected to Kafka successfully.")
            return producer
        except Exception as e:
            logger.error("Connection to Kafka failed. Error: %s" % str(e))
            time.sleep(retry_sleep)
            retry_count += 1
            if retry_count < max_retries:
                logger.info(f"Retrying to create Kafka producer (Attempt {retry_count}/{max_retries})")
                time.sleep(retry_sleep)
            else:
                logger.error("Maximum retry count reached. Unable to connect to Kafka.")
                raise e
producer = kafka_producer_initialize()


# Your functions here to handle your endpoints

def receive_post_event(body):
    ''' Receives a post event '''

    trace_id = str(uuid.uuid4())

    logger.info(f"Received event postEvent request with a trace ID of {trace_id}")

    body['trace_id'] = trace_id

    msg = {
        "type": "PostEvent",
        "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
        "payload": body
    }

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f"Returned event postEvent response (ID: {trace_id}) with status 201")

    return NoContent, 201

def receive_interaction_event(body):
    ''' Receives an interaction event '''

    trace_id = str(uuid.uuid4())

    logger.info(f"Received event interactionEvent request with a trace ID of {trace_id}")

    body['trace_id'] = trace_id

    msg = {
        "type": "InteractionEvent",
        "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
        "payload": body
    }

    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    logger.info(f"Returned event interactionEvent response (ID: {trace_id}) with status 201")

    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080, host="0.0.0.0")